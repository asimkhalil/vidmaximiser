package  
{ 
	import com.app.fb.fb.FacebookCenter;
	
	import mx.collections.ArrayCollection;

	public class AppSingleton
	{
		[Bindable]
		public static var appId:String = "";
		[Bindable]
		public static var secret:String = "";
		[Bindable]
		public static var pageId:String = "";
		/*[Bindable]
		public static var log:ArrayCollection = new ArrayCollection();*/
		[Bindable]
		public static var userProfilePhoto:String;
		[Bindable]
		public static var facebookUserName:String;
		[Bindable]
		public static var facebookUserProfileId:String;
		[Bindable]
		public static var fanPageSelectedIndex:int;
		[Bindable]
		public static var fb:FacebookCenter;
		public static var files:ArrayCollection = new ArrayCollection();
		public static var postDate:Date;
		public static var postGap:int = 4;
		
		public static var postGapOptions:int = 0;
		
		[Bindable]
		public static var logs:ArrayCollection;
		
		[Bindable]
		public static var exclusions:ArrayCollection = new ArrayCollection();
		
		public static var macAddress:String = "";
		
		
		public static function addExclusion(exclusion:String):void {
			if(exclusions.contains(exclusion) == false) {
				exclusions.addItem(exclusion);
			}
		}
		
		public static function removeExclusion(exclusion:String):void {
			if(exclusions.contains(exclusion) == true) {
				exclusions.removeItem(exclusion);
			}
		}
	}
}