package com.app
{
	public class Utils
	{
		public static function getHexColor(color1:uint):String {
			var color:String = color1.toString(16);
			if (color.length < 6){
				var razlika:int = 6-color.length;
				var temp_color:String = '';
				for (var i:int=0; i<razlika; i++){
					temp_color += '0';
				}
				temp_color += color;
				color = temp_color; 
			}
			return color;
		}
	}
}