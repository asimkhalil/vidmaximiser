package com.app.events
{
	import flash.events.Event;
	import flash.utils.ByteArray;
	
	public class ImageSelectedEvent extends Event
	{
		public var imageData:ByteArray;
		
		public function ImageSelectedEvent(type:String, imageData:ByteArray, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.imageData = imageData;
			super(type, bubbles, cancelable);
		}
	}
}