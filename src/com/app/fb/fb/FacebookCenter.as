package com.app.fb.fb
{
	import com.app.vo.FacebookPageVO;
	import com.facebook.graph.FacebookDesktop;
	import com.facebook.graph.data.FacebookSession;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.errors.IOError;
	import flash.events.*;
	import flash.filesystem.File;
	import flash.geom.Matrix;
	import flash.net.FileReference;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.events.*;
	import mx.formatters.DateFormatter;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	
	public class FacebookCenter extends EventDispatcher {
		[Bindable]
		public var isLoggedIn:Boolean;
		[Bindable]
		public var myPages:Array;
		private const insighths_params:Array=["post_impressions_unique","post_engaged_users","post_stories"," post_storytellers"," post_negative_feedback_unique"];
		//Live app key
		private var APP_ID:String 					=""//White Label;
		private var APP_SECRET:String = "";
		private const FACEBOOK_APP_ORIGIN:String 		= "http://avectris.com/";
		private const PERMISSIONS:Array 				= ["manage_pages", "read_insights", "publish_actions", "publish_pages"];
		public var accessToken:String 					= "";
		[Bindable] public var facebookPagesObjects:Object;
		[Bindable] public var facebookPagesArray:Array;
		[Bindable] public var postListPageNumber:int = 0;
		[Bindable] public var postListPageSize:int = 16;
		[Bindable] public var postListReachedEnd:Boolean;
		[Bindable] public var postsArray:Array;
		[Bindable] public var recentPostsArray:ArrayCollection = new ArrayCollection();
		
		/* Constants */
		private const GROUP_CREATED:int = 11; //  - Group created
		private const EVENT_CREATED:int = 12 ; //- Event created
		private const STATUS_UPDATE:int = 46 ; //- Status update
		private const POST_ON_WALL_FROM_ANOTHER_USER:int = 56; // - Post on wall from another user
		private const NOTE_CREATED:int = 66; // - Note created
		private const LINK_POSTED:int = 80; // - Link posted
		private const VIDEO_POSTED:int = 128; // -Video posted
		private const PHOTO_POSTED:int = 247; // - Photos posted
		private const APP_STORY_1:int = 237; // - App story
		private const COMMENT_CREATED:int = 257; // - Comment created
		private const APP_STORY_2:int = 272; // - App story
		private const CHECKIN_TO_PLACE:int = 285; // - Checkin to a place
		private const POST_IN_GROUP:int = 308; // - Post in Group
			
		[Bindable] public var FacebookPostTypesOptions:Array = 
		[
			{label:"Group created", data:"11"},
			{label:"Event created", data:"12"},
			{label:"Status update", data:"46"},
			{label:"Post on wall from another user", data:"56"},
			{label:"Note created", data:"66"},
			{label:"Link posted", data:"80"},
			{label:"Video posted", data:"128"},
			{label:"Photos posted", data:"247"},
			{label:"App story", data:"237"},
			{label:"Comment created", data:"257"},
			{label:"App story", data:"272"},
			{label:"Checkin to a place", data:"285"},
			{label:"Post in Group", data:"308"}
		];
		
		// graph.facebook.com/<pageid>/promotable_posts
		public function FacebookCenter(appid:String = "",secret:String = "") {
			this.APP_ID = appid;
			this.APP_SECRET = secret;
			//super(this);
			init();
		}
		
		/* Init facebook connectivity */
		public function init():void {
			FacebookDesktop.init(this.APP_ID, facebookInitResult,this.accessToken);
		}
		
		private function facebookInitResult(success:Object, fail:Object):void {
			if(!success) trace("Init failed") else trace("init success");
			
		}
		
		/* Login to facebook and result*/
		public function login():void {
			FacebookDesktop.login(logInFacebookResult, PERMISSIONS);
		}
		
		private function logInFacebookResult(success:Object, fail:Object):void {
			
			if(!success) {
				trace("Login failed") 
			} else {
				accessToken = success.accessToken;
				grabLongLivedToken(onDone);
				isLoggedIn=true;
				function onDone(token:String):void{
					if(token&&token!="")
						accessToken=token;
					dispatchEvent(new Event("LOGGED_IN")); 
					
				}
				//trace("user access token received :" + accessToken);
				var userProfilePhotoUrl:String = FacebookDesktop.getImageUrl(success.uid,"small");
				AppSingleton.userProfilePhoto = userProfilePhotoUrl;
				AppSingleton.facebookUserName = success.user.name;
				AppSingleton.facebookUserProfileId = success.user.id;
			}
		}
		private var longLivedTokenResponseObtained:Boolean=false;
		private function grabLongLivedToken(onDone:Function):void{
			var urlLoader:URLLoader=new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE, completeHandler);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ioErrorHandler);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			var session:FacebookSession=FacebookDesktop.getSession();
			var url:String="http://engagehook.com/app/facebookaccess_commentmaximizer.php?token="+session.accessToken+"&appid="+APP_ID+"&appsecret="+APP_SECRET;
			urlLoader.load(new URLRequest(url));
			function completeHandler(e:Event):void{
				var access_token:String=StringUtil.trim(urlLoader.data.toString());
				//ther is an issue where I get the complete event 2 times, first time the token is emptu
				if(access_token==""||longLivedTokenResponseObtained)
					return;
				longLivedTokenResponseObtained=true;
				
				urlLoader.removeEventListener(Event.COMPLETE, completeHandler);
				urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, ioErrorHandler);
				urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
				session.accessToken=access_token;
				if(onDone!=null)
					onDone(access_token);
			}
			function ioErrorHandler(e:Event):void{
				trace("error getting long lived access token");
				urlLoader.removeEventListener(Event.COMPLETE, completeHandler);
				urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, ioErrorHandler);
				urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
				
				onDone(null);
			}
		}
		public function getUserPagesList():void {
			var params:Object = {};
			params.access_token = this.accessToken;
			params.limit = 1000;
			FacebookDesktop.api("/me/accounts/", getUserPagesCallResult, params);
		}
		
		public function getUserPagesCallResult(success:Object, fail:Object):void {
			//trace("Session = " + FacebookDesktop.);
			if(success) {
				facebookPagesObjects = success;
				loadPagesArrayFromResult(success);
				dispatchEvent(new Event("PAGES_ARRAY_LOADED"));
				trace("get status success ");
			} else {
				trace("get status failed");
			}
		}
		
		private function loadPagesArrayFromResult(pData:Object):void {
			facebookPagesArray = new Array();
			
			//Profile
			/*var profileTemp:FacebookPageVO = new FacebookPageVO();
			profileTemp.id = Singleton.facebookUserProfileId;
			profileTemp.label = "Personal Profile";
			profileTemp.name = "Personal Profile";
			profileTemp.data = Singleton.facebookUserProfileId;
			facebookPagesArray.push(profileTemp);*/
			
			var tempPage:FacebookPageVO;
			
			var tempArray:Array = new Array();
			
			for each (var item:Object in pData) {
				tempPage = new FacebookPageVO();
				tempPage.loadDataFromResult(item, true);
				tempPage.label = tempPage.name;
				//trace("received access_token for page name=" + tempPage.label + " with token = " + tempPage.access_token);
				tempPage.data  = tempPage.id.toString();
				facebookPagesArray.push(tempPage);
			}
			
			if(facebookPagesArray.length > 0) { 
				AppSingleton.pageId = facebookPagesArray[0].id;
			}
			
			//FlexGlobals.topLevelApplication.cmbFanPage.selectedIndex = 0;
			
			//FlexGlobals.topLevelApplication.cmbFanPage.dataProvider = new ArrayCollection(facebookPagesArray);
			
			loadUserLikesForFanPages(facebookPagesArray);
			
			if(facebookPagesObjects.length > 0) {
				populateFanPageThumbnail();	
			} else {
				userPagesFaultHandler();
			}
		}
		
		private var fanPageCount:int = 1;
		
		private function populateFanPageThumbnail():void {
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			urlLoader.addEventListener(Event.COMPLETE,completeHnd);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR,function(event:Event):void {
				urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
				facebookPagesArray[fanPageCount-1].thumb = null; 
				if(fanPageCount < facebookPagesArray.length) {
					fanPageCount++;
					populateFanPageThumbnail();
				} else {
					return;
				}
			});
			urlLoader.load(new URLRequest('https://graph.facebook.com/v2.7/'+facebookPagesArray[fanPageCount-1].id+'/picture'))	
			
			function completeHnd(event:Event):void {
				urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
				facebookPagesArray[fanPageCount-1].thumb = event.target.data; 
				if(fanPageCount < facebookPagesArray.length) {
					fanPageCount++;
					populateFanPageThumbnail();
				} else {
					return;
				}
			}
		}	
		
		private var fanPageCountForStatistics:int = 1;
		
		private function populateFanPageThumbnailForStatistics():void {
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			urlLoader.addEventListener(Event.COMPLETE,completeHnd);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR,function(event:Event):void {
				urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
				myPages[fanPageCountForStatistics-1].thumb = null; 
				if(fanPageCountForStatistics < myPages.length) {
					fanPageCountForStatistics++;
					populateFanPageThumbnailForStatistics();
				} else {
					return;
				}
			});
			urlLoader.load(new URLRequest('https://graph.facebook.com/v2.7/'+myPages[fanPageCountForStatistics-1].id+'/picture'))	
			
			function completeHnd(event:Event):void {
				urlLoader.removeEventListener(Event.COMPLETE,completeHnd);
				myPages[fanPageCountForStatistics-1].thumb = event.target.data; 
				if(fanPageCountForStatistics < myPages.length) {
					fanPageCountForStatistics++;
					populateFanPageThumbnailForStatistics();
				} else {
					FlexGlobals.topLevelApplication.dispatchEvent(new Event("STATISTICS_PAGES_ARRAY_LOADED",true));
					return;
				}
			}
		}	
		
		private function userPagesFaultHandler():void {
			FlexGlobals.topLevelApplication.enableApp();
			Alert.show("Could not load fan pages data from facebook");
			/*var popup:FacebookDataFaultPopup = new FacebookDataFaultPopup();
			PopUpManager.addPopUp(popup,FlexGlobals.topLevelApplication as DisplayObject,true);
			PopUpManager.centerPopUp(popup);*/
		}
		
		public function loadUserLikesForFanPages(pages:Array):void {
			var count:int = 0;
			getUserLikes(pages[count].id,userLikesLoaded);
				
			function userLikesLoaded(success:Event):void {
				userLikesLoader.removeEventListener(Event.COMPLETE,userLikesLoaded);
				if(success.target) {
					if(pages[count].name != "Personal Profile") {
						var result:Object = JSON.parse(success.target.data.toString());
						if(result.fan_count != undefined) {
							pages[count].name += " ("+result.fan_count+" fans)";
						} else {
							pages[count].name += " (0 fans)";
						}
					}
					if(count < pages.length-1) {
						count++;
						getUserLikes(pages[count].id,userLikesLoaded);	
					}
				}
			}
		}
		
		var userLikesLoader:URLLoader;
		
		public function getUserLikes(id:String,callback:Function):void {
			var url:String = "https://graph.facebook.com/"+id+"?fields=fan_count&access_token="+this.accessToken;
			userLikesLoader = new URLLoader();
			userLikesLoader.addEventListener(Event.COMPLETE,callback);
			userLikesLoader.load(new URLRequest(url));
			/*FacebookDesktop.api("/"+id+"/?fields=likes&access_token="+this.accessToken,callback);*/
		}

		/* Logout of facebook and result */
		public function logout():void {
			FacebookDesktop.logout(logOutOfFacebookResult);
		}
		
		public function logOutOfFacebookResult(success:Boolean):void {
			
			if(success)
				FacebookDesktop.login(logInFacebookResult, PERMISSIONS);
			else
				FacebookDesktop.login(logInFacebookResult, PERMISSIONS);
		}
		
		public function logoutApp():void {
			FacebookDesktop.logout(logOutOfFacebookAppResult);
		}
		
		public function logOutOfFacebookAppResult(success:Boolean):void {
			if(success)
				dispatchEvent(new Event("LOG_OUT_SUCCESS"));
			else
				trace("Could not log out");
		}
		
		/* Get status and result */
		public function testFunction():void {
			var params:Object = {};
			params.action_token = this.accessToken;		
			
		}
		
		/* pTime has to be empty or in mysql datetime format: 2013-06-01 23:23:00 */
		public function postLinkToPageWall(personalProfile:Boolean,pPageId:String, pText:String, pAccessToken:String, pPostTime:int,scheduledTS:Date, pLink:String = "", pSource:String = "", pThumbnailURL:* = "",target:String=""):void {
			var params:Object = {};
			
			var apiMethod:String = ( pPageId + "/feed");
			
			if(personalProfile) {
				apiMethod = ( "/"+pPageId + "/feed");
			}
			
			params.message = pText;
			if(target != null && target != "") {
				var targeting:String = '{"countries":["'+target+'"]}';
				params.targeting = targeting;
			}
			params.access_token = personalProfile?accessToken:pAccessToken;
			
			if (pLink) {
				params.link		= pLink;   
			}
			
			if (pThumbnailURL) {
				if(pThumbnailURL is String) {
					if (pThumbnailURL.length > 10) {
						params.picture	= pThumbnailURL;	
					} 
				} else {
					params.picture	= pThumbnailURL;
					params.fileName = "CustomThumb.png";
				}
			}
			
			// If this is a scheduled post then mark it as such here.
			if (pPostTime > 0) {
				params.published = false;
				//var date:Date = QuickDateFormatter.convertStringToDateTime(pPostTime);
				//var utc:Number = Math.round(date.time/1000);
				//params.scheduled_publish_time = utc;
				params.scheduled_publish_time = pPostTime;
			}	
											
			FacebookDesktop.api(apiMethod , postTextToPageWallResult, params, "POST");
			/*var scheduledPostVo:ScheduledPostVO = new ScheduledPostVO();
			scheduledPostVo.post_name = "scheduled post";
			scheduledPostVo.post_type = 1;
			scheduledPostVo.scheduled_date = convertToSqlTimeStamp(scheduledTS);
			scheduledPostVo.status = 0;
			scheduledPostVo.entered_date = convertToSqlTimeStamp(new Date());
			scheduledPostVo.content_text = pText;
			scheduledPostVo.content_link_url = pLink;
			scheduledPostVo.video_url = pSource;
			scheduledPostVo.saveVO();*/
		}
		
		private function convertToSqlTimeStamp(dateTime:Date):String {
			var df:DateFormatter = new DateFormatter();
			df.formatString = "YYYY-MM-DD JJ:NN:SS";
			return df.format(dateTime);
		}
		
		public function postImageToPageWall(personalProfile:Boolean,pPageId:String, pText:String, pFileName:String, 
											pFile:Object, pAccessToken:String, pPostTime:int,postTS:Date,target:String = ""):void {
			var params:Object = new Object();
			
			var apiMethod:String = ( pPageId + "/photos");
			
			if(personalProfile) {
				apiMethod = ( "/"+pPageId + "/photos");
			}
			
			params.message 	= pText;
			params.source	= pFile;
			
			
			// If this is a scheduled post then mark it as such here.
			if (pPostTime > 0) {
				params.published = false;
				//var date:Date = QuickDateFormatter.convertStringToDateTime(pPostTime);
				//var utc:Number = Math.round(date.time/1000);
				params.scheduled_publish_time = pPostTime;
			}	
			
			if(target != null && target != "") {
				var targeting:String = '{"countries":["'+target+'"]}';
				params.targeting = targeting;
			}
			
			params.access_token = personalProfile?accessToken:pAccessToken;
			
			/*var scheduledPostVo:ScheduledPostVO = new ScheduledPostVO();
			scheduledPostVo.post_name = "scheduled post";
			scheduledPostVo.post_type = 1;
			scheduledPostVo.scheduled_date = convertToSqlTimeStamp(postTS);
			scheduledPostVo.status = 0;
			scheduledPostVo.entered_date = convertToSqlTimeStamp(new Date());
			scheduledPostVo.content_text = pText;
			scheduledPostVo.content_link_url = pFileName;
			scheduledPostVo.video_url = pFileName;
			scheduledPostVo.saveVO();*/
			
			FacebookDesktop.api( apiMethod, postImageToPageWallResult, params, "POST");
		}
		
		public function postImageToPageWallResult(success:Object, fail:Object):void {
			//trace("Session = " );
			if(success) {
				
				if(success["error"] != undefined) 
				{
					Alert.show(success["error"].message);
					dispatchEvent(new Event("WALL_IMAGE_POST_SEND_FAILED"));	
					return;
				}
				trace("Publish image to fanPage success ");
				dispatchEvent(new DataEvent("WALL_IMAGE_POST_SENT",true,false,success.id));
			} else {
				Alert.show("Publish image to fanPage failed");
				dispatchEvent(new Event("WALL_IMAGE_POST_SEND_FAILED"));	
			}
		}
		
		public function postTextToPageWallResult(success:Object, fail:Object):void {
			//trace("Session = " + FacebookDesktop.);
			if(success) {
				if(success["error"] != undefined) 
				{
					Alert.show(success["error"].message);
					dispatchEvent(new Event("WALL_TEXT_POST_SEND_FAILED"));	
					return;
				}
				//trace("Publish to fanPage success ");
				/*Singleton.log.addItem("https://www.facebook.com/"+success.id);*/
				dispatchEvent(new DataEvent("WALL_TEXT_POST_SENT",true,false,success.id));
			} else {
				//trace("Publish to fanPage failed " + fail.error.message);
				Alert.show("Publish link to fanPage failed");
				dispatchEvent(new Event("WALL_TEXT_POST_SEND_FAILED"));	
			}
		}		
		
		private var postSummaryUrlLoader:URLLoader;		
		
		public function getPostsSummary(pageId:String,profile:Boolean):void {
			var params:Object = {};
			params.access_token = this.accessToken;
			if(profile) {
				FacebookDesktop.api("/"+pageId+"/feed", getAllProfilePostsResult, params);
			} else {
				FacebookDesktop.api("/"+pageId+"/feed", getAllPostsResult, params);
			}
		}	
		
		private function filterQilioPosts(item:Object):Boolean {
			if(item.application != undefined && item.application.name == "Qilio") {
				return true;
			} 
			return false;
		}
		
		public function getAllProfilePostsResult(success:Object, fail:Object):void {
			if(success) {
				var result:ArrayCollection = new ArrayCollection(success as Array);
				
				result.filterFunction = filterQilioPosts;
				
				result.refresh();
				
				recentPostsArray.removeAll();
				if(result.length > 20) {
					for(var i:int=0;i<20;i++) {
						recentPostsArray.addItem(result.getItemAt(i));
					}
				} else {
					recentPostsArray = new ArrayCollection(result.source);
				}
				recentPostsArray.refresh();
				trace("get status success ");
			} else {
				trace("get status failed");
			}
		}
		
		private function filterOutComments(item:Object):Boolean {
			if(item.type.toString().toLowerCase() == "status" && item.status_type == undefined) {
				return false;
			}
			return true;
		}
		
		public function getAllPostsResult(success:Object, fail:Object):void {
			if(success) {
				var result:Array = success as Array;
				recentPostsArray.removeAll();
				recentPostsArray.filterFunction = filterOutComments;
				if(result.length > 20) {
					for(var i:int=0;i<20;i++) {
						recentPostsArray.addItem(result[i]);
					}
				} else {
					recentPostsArray = new ArrayCollection(result);
				}
				recentPostsArray.refresh();
				trace("get status success ");
			} else {
				trace("get status failed");
			}
		}
		
		public function getAllPagePosts(pPageId:String, pFromDate:int, pToDate:int, pPageStep:int):void {
			
			if (postListReachedEnd && pPageStep == 1) {
				//trace("There are no more records to show, returning");
				
				dispatchEvent(new Event("POSTS_FETCHING_REACHED_END"));	
				return;
			}
			
			if (this.postListPageNumber == 0 && pPageStep == -1) {
				dispatchEvent(new Event("POSTS_FETCHING_REACHED_BEGINNING"));
				trace("We are already in the first page");
				return;
			}
			
			if (pPageStep == 1) {
				this.postListPageNumber++;
			} else if (pPageStep == -1 && this.postListPageNumber > 0) {
				this.postListPageNumber--;
			}
			
			var query:String = "select " +
			"							post_id, " +
			"							source_id, " +
			"							message, " +
			"							attachment, " +
			"							created_time, " +
			"							description, " +
			"							is_published, " +
			"							message, " +
			"							scheduled_publish_time, " +
			"							type, " +
			"							updated_time, " +
			"							permalink " +
			"					from " +
			"							stream " +
			"					where  " + 
			"							source_id=" + pPageId + " and " +
			"							(is_published=0 or is_published=1) and " +
			"							created_time >= " + pFromDate.toString() + " and " +
			"							created_time <= " + pToDate.toString() + 
			"				 order by 	is_published " +
			"					limit " + (postListPageNumber * postListPageSize).toString() + "," + postListPageSize.toString();
			
			//trace("query = " + query);
			
			FacebookDesktop.fqlQuery(query, gotPostDataResponse);
		}
		
		/*
			To publish an unpublished post, simply set the is_published variable to true via an API call.
			
			When changing the schedule of a post, the new timestamp value should be specified as one of the following:
			
			A valid scheduled publish time value: this will set the post to be published at the specified time. If the post was never scheduled before, 
			it will now become scheduled. If the post had been scheduled before, it will be rescheduled for the new time.
			0: this will cancel the scheduled post, turning it into an unpublished post with no associated schedule publish time. Scheduled posts cannot 
			be canceled or rescheduled within 3 minutes of their scheduled publish time (these requests will fail). The new scheduled publish time must be 
			greater than 10 minutes from now and less than 6 months from now.
		
		FB.api('/<post_id>',
		'POST',
		{
		access_token: '{page access token}',
		backdated_time: 1369850939,
		},
		function(response) {
		 
			}
		);  
		*/
		
		public function unpublishScheduledPost(pPageId:String, pPostId:String, pAccessToken:String):void {
			var params:Object = {};
			var apiMethod:String = ( "/" + pPostId );
			
			//params.is_published = false;
			params.access_token = pAccessToken;
			params.scheduled_publish_time = 0;
		
			params.post_id = pPostId;
			FacebookDesktop.api(apiMethod , postUnpublishPostResult, params, "POST");
		}
		
		// if pNewUnixTime == 0 then the post will be published inmediatly
		public function reSchedulePost(pPageId:String, pPostId:String, pNewUnixTime:int, pAccessToken:String):void {
			var params:Object = {};
			var apiMethod:String = ( "/" + pPostId );
			params.access_token = pAccessToken;
			
			if (pNewUnixTime == 0) {
				params.is_published = true;
			} else {
				//params.is_published = false;
				params.post_id = pPostId;
				params.scheduled_publish_time = pNewUnixTime;
			}
			
			params.access_token = pAccessToken;
			
			FacebookDesktop.api(apiMethod , reSchedulePostResult, params, "POST");
		}
		
		private function postUnpublishPostResult(success:Object, failed:Object):void {
			if (success)
				dispatchEvent(new Event("UNPUBLISH_POST_SUCESS"));	
			else
				dispatchEvent(new Event("UNPUBLISH_POST_FAILED"));	
		}
		
		private function reSchedulePostResult(success:Object, failed:Object):void {
			if (success)
				dispatchEvent(new Event("RESCHEDULE_POST_SUCCESS"));	
			else
				dispatchEvent(new Event("RESCHEDULE_POST_FAILED"));
		}
		
		private function gotPostDataResponse(pResponse:Object, pSomething:Object):void {
			if (pSomething) {
				Alert.show("There has been an error fecthing data from Facebook");
				return;
			}
			postsArray = pResponse as Array;
			postListReachedEnd = postsArray.length == 0;
			
			if (postListReachedEnd) {
				//trace("Reached end of list");
				if (this.postListPageNumber > 1) {
					this.postListPageNumber--;	
				}
				
				dispatchEvent(new Event("POSTS_FETCHING_REACHED_END"));	
			} else {
				dispatchEvent(new Event("POSTS_FETCHING_SUCCESS"));	
			}
			
		}
		
		public static function getUnixStampTime(pDate:Date):int {
			var utc:Number = Math.round(pDate.time/1000);
			return utc;
		}
		/*public function loadPosts(page:Page,onDone:Function=null,since:Date=null,until:Date=null,getStats:Boolean=true):void{
			var query:String=page.id+"/posts";//?limit=5&since=1298995597"
			var  params:*=new Object();
			params.access_token=page.access_token;
			params.limit=100;
			if(since)
				params.since=since;
			if(until)//because time zone or other issues you will not see what you just posted so add 1 day to the date to be sure
			{
				var now:Date=new Date();
				if(until.date==now.date&&until.month==now.month&&until.fullYear==now.fullYear)
					until=new Date(now.fullYear,now.month,now.date+1);
				params.until=until;
			} 
			
			FacebookDesktop.api(query,onQueryDone,params);
			function onQueryDone(res:*,res2:*):void{
				var arr:Array=(res) as Array;
				if(!arr)arr=[];
				arr=arr.map(Post.fromObject);
				if(getStats)
					arr.forEach(getStatsForPost);
				page.posts=new ArrayCollection(arr);
				if(onDone!=null)
					onDone(page);
			}
		}*/
		/*public function getStatsForPost(post:Post,...rest):void{
			var query:String=post.getId()+"/insights/";
			FacebookDesktop.api(query,onPageLoaded);
			
			function onPageLoaded(e:Object,res2:*=null):void{
				var data:Array= e as Array;
				if(!data) return;
				//filter to remove what we do not need
				//data=data.filter(filterData).map(StatisticItem.fromObject);
				var ps:PostStatistics= PostStatistics.fromArrayOfResults(data);
				post.setPostStatistics(ps);
			}
			function filterData(o:Object,...rest):Boolean{
				return insighths_params.indexOf(o["name"])>=0;
			}
		}
		*/
		/*public function deletePost( post:Post,onDone:Function):void {
			var params:Object = {};
			var apiMethod:String = ( "/" + post.id );
			params.access_token = post.page.access_token;
			var pNewUnixTime:int=0;
			params.access_token = post.page.access_token;
			params.method="delete";
			FacebookDesktop.api(apiMethod , onDoneHandler, params, "POST");
			function onDoneHandler(res:*,fault:*=null):void{
				if(onDone!=null)
					onDone(res);
			}
		}*/
		
		/*public function reSchedulePostByDate( post:Post, date:Date,onDone:Function):void {
			var params:Object = {};
			var apiMethod:String = ( "/" + post.getId() );
			params.access_token = post.page.access_token;
			var pNewUnixTime:int=0;
			if(date)
				pNewUnixTime=getUnixStampTime(date);
			if (pNewUnixTime == 0) {
				params.is_published = true;
			} else {
				//params.is_published = false;
				params.post_id = post.getId();
				params.scheduled_publish_time = pNewUnixTime;
			}
			
			params.access_token = post.page.access_token;
			FacebookDesktop.api(apiMethod , onDoneHandler, params, "POST");
			function onDoneHandler(res:*,fault:*=null):void{
				if(onDone!=null)
					onDone(res);
			}
		}*/
		
		/*public function loadPages(onDone:Function=null):void{
			var params:Object = {};
			params.access_token = this.accessToken;
			params.limit = 1000;
			var query:String="me/accounts";
			FacebookDesktop.api(query,onQueryDone,params);
			function onQueryDone(res:*,res2:*):void{
				var arr:Array=(res) as Array;
				if(!arr)arr=[];
				arr=arr.map(Page.fromObject);
				myPages=arr;
				if(onDone!=null)
					onDone(myPages);
				
				loadUserLikesForFanPages(myPages);
				
				populateFanPageThumbnailForStatistics();
			}
		}*/
		/*public function loadPostsFQLWithStats(page:Page,onDone:Function=null,since:Date=null,until:Date=null,byCreationDate:Boolean=true):void{
			loadPostsFQL(page,onCompleteHandler,since,until,byCreationDate);
			function onCompleteHandler(page:Page,...rest):void{
				if(page && page.posts) {
					page.posts.refresh();
					for(var i:int=0;i<page.posts.length;i++) {
						getStatsForPost(page.posts.getItemAt(i) as Post);
					}
					page.posts.refresh();
					if(onDone!=null)
						onDone(page);
				}				
			}
		}*/
		
		public function replaceAll(str,replace:String, replaceWith:String):String {
			str = str.replace(new RegExp(replace, 'g'), replaceWith);
			return str;
		}
		
		/*public function loadPostsFQL(page:Page,onDone:Function=null,since:Date=null,until:Date=null,byCreationDate:Boolean=true):void{
			
			var fields:String = "&fields=application,from,likes,updated_time,type,shares,id,message,created_time,link,comments,status_type,object_id";
			
			var query:String = "";
			
			if(byCreationDate) {
				query = "https://graph.facebook.com/v2.7/"+page.id+"/posts?limit=100&access_token="+this.accessToken+"&since="+getUnixStampTime(since)+"&until="+getUnixStampTime(until);
				
				query = query+fields+"&summary=true";	
				
			} else {
				fields += ",scheduled_publish_time";
				
				query = "https://graph.facebook.com/v2.7/"+page.id+"/promotable_posts?limit="+ApplicationModel.loadingOffsetInterval+"&is_published=false&access_token="+this.accessToken;
				
				query = query+fields+"&summary=true&offset="+ApplicationModel.loadingOffset;
			}
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.addEventListener(Event.COMPLETE,function(event:Event):void {
				var jsonResponse:Object = JSON.parse(event.target.data);
				var arr:Array=jsonResponse.data;
				if(arr.length > 0) {
					if(!arr)arr=[];
					
					arr=arr.map(Post.fromObject);
					
					arr.forEach(function(p:Object,...rest):void{p.page=page});
					if(!byCreationDate && ApplicationModel.loadingOffset > 0) {
						page.posts.addAll(new ArrayCollection(arr));
					} else {
						page.posts=new ArrayCollection(arr);
					}
					page.posts.filterFunction = filterPostsFunction;
					page.posts.refresh();
				} else {
					Alert.show("No Sceduled Posts Found!","Information");
					CursorManager.removeBusyCursor();
				}
				if(onDone!=null)
					onDone(page);
			});
			urlLoader.load(new URLRequest(query));*/
			
			/*function onQueryDone(res:Object,fail:Object):void{ */
				//trace(res.target.data);
				/*var arr:Array=(res) as Array;
				if(!arr)arr=[];
				arr=arr.map(Post.fromObject);
				
				arr.forEach(function(p:Post,...rest):void{p.page=page});
				page.posts=new ArrayCollection(arr);
				if(onDone!=null)
					onDone(page);	*/
			/*}*/
		/*}*/
		
		public function uploadVideo(targetId:String, 
									title:String, 
									description:String, 
									fileName:String, 
									video:ByteArray, 
									scheduleTime:int = -1, 
									pageAccessToken:String = "", 
									dtStamp:String = "",
									scheduledDate:Date = null):void 
		{
			var params:Object = {};
			params.title = title;
			params.description = description;
			params.fileName = fileName+".mp4";
			params.video = video;
			params.access_token = pageAccessToken;
			FacebookDesktop.uploadVideo("/"+targetId+"/videos", uploadDone, params);
			
			function uploadDone(success:Object, fail:Object):void 
			{
				if(success) 
				{
					
					postLinkToPageWall(false, targetId, title, pageAccessToken, scheduleTime, scheduledDate, 
						"https://www.facebook.com/"+targetId+"/videos/"+success.id+"/");  
					
					var apiMethod:String = ( targetId + "/feed"); 
				}
				else 
				{
					CursorManager.removeBusyCursor();
					dispatchEvent(new Event("WALL_TEXT_POST_SEND_FAILED"));
					//Alert.show("Facebook upload failed! "+ObjectUtil.toString(fail));
					
				}
			}
			
			function postVideoToWallResult(success:Object, fail:Object):void 
			{
				if(success) 
				{
					CursorManager.removeBusyCursor();
					dispatchEvent(new Event("VIDEO_POST_SUCCSSFULLY"));
				}
				else 
				{
					CursorManager.removeBusyCursor();
					dispatchEvent(new Event("VIDEO_POST_FAILED"));
				}
			}
		}
		
		private function filterPostsFunction(item:Object):Boolean {
			if(!(item.type == "status" && item.status_type == undefined)) {
				return true;
			}
			return false;
		}
	}
}