package com.app.util
{
	public class EmoticonsUtil
	{
		private static var _instance:EmoticonsUtil = new EmoticonsUtil();
		
		public function EmoticonsUtil()
		{
			if (_instance != null)
			{
				throw new Error("Singleton can only be accessed through Singleton.instance");
			}
			emoticons[":-)"] = "assets/images/smiley/1.png";
			emoticons[":-("] = "assets/images/smiley/2.png";
			emoticons[":-*"] = "assets/images/smiley/3.png";
			emoticons["(nerd)"] = "assets/images/smiley/4.png";
			emoticons[":-D"] = "assets/images/smiley/5.png";
			emoticons["):|"] = "assets/images/smiley/6.png";
			emoticons["B-)"] = "assets/images/smiley/7.png";
			emoticons["(rofl)"] = "assets/images/smiley/8.png";
			emoticons[":v)"] = "assets/images/smiley/9.png";
			emoticons[":-P"] = "assets/images/smiley/10.png";
			emoticons["(worry)"] = "assets/images/smiley/11.png";
			emoticons["(angry)"] = "assets/images/smiley/12.png";
			emoticons[":-X"] = "assets/images/smiley/13.png";
		}
		
		public static function get instance():EmoticonsUtil
		{
			return _instance;
		}
		
		public var emoticons:Array = new Array();
	}
}