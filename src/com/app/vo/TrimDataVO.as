package com.app.vo
{
	public class TrimDataVO
	{
		public function TrimDataVO(fileName:String, startTime:String, endTime:String)
		{
			this.fileName = fileName;
			this.startTime = startTime;
			this.endTime = endTime;
		}
		
		public var fileName:String = "";
		public var startTime:String = "";
		public var endTime:String = "";
	}
}