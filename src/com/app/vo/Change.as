package com.app.vo
{
	import flash.display.DisplayObject;

	public class Change
	{
		public var type:String = '';
		public var oldBackgroundColor:uint;
		public var newBackgroundColor:uint;
		public var changedObject:DisplayObject;
		public var oldX:int = -1;
		public var oldY:int = -1;
		public var oldW:int = -1;
		public var oldH:int = -1;
		public var oldRotation:int = -1;
		
		public var newX:int = -1;
		public var newY:int = -1;
		public var newW:int = -1;
		public var newH:int = -1;
		public var newRotation:int = -1;
		
		public var oldSource:Object;
		public var newSource:Object;
		
		public var oldFilters:Array;
		public var newFilters:Array;
	}
}