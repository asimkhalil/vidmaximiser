package com.app.vo {
	[Bindable]
	public class TextPropertiesVO {
		public function TextPropertiesVO(text:String = "Enter text here", 
										 textColor:uint = 0x000000, 
										 font:String = "Arial", 
										 textAlign:String = "Center", 
										 bgColor:uint = 0xFFFFFF, 
										 fontSize:int = 50, 
										 transparent:Boolean = true) {
			
			this.textColor = textColor;
			this.font = font;
			this.textAlign = textAlign;
			this.bgColor = bgColor;
			this.fontSize = fontSize;
			this.transparent = transparent;
		}
		
		public var text:String = "Enter text here";
		public var textColor:uint = 0x000000;
		public var font:String = "Arial";
		public var textAlign:String = "Center";
		public var bgColor:uint = 0xFFFFFF;
		public var fontSize:int = 50;
		public var transparent:Boolean = true;
	}
}