package com.app.vo
{
	import flash.events.*;
	import flash.net.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.events.*;
	import mx.managers.CursorManager;
	import mx.messaging.messages.RemotingMessage;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	public class VOBaseCode extends EventDispatcher
	{
		public var readOnlyVO:Boolean = false;
		
		/*  Mandatory properties for all VOs */
		public var tableOrView:String;
		public var searchField:String;
		public var VOStoredProcedure:String;
		
		/* These are for comboboxes data filling option */
		public var label:String;
		public var data:String;
		
		/* VO objects properties */
 		
		public function VOBaseCode(){
			
		}
		
		/* Binary Proxy. Pass this function an object with the relevant data and if this object has such property it will be given the value of the object.*/
		public function loadDataFromResult(dataObject:Object,  displayLog:Boolean = false):void {
			
			for (var property:String in dataObject) {
				if (displayLog == true) {
					//trace("processing property ");
				}
				if (this.hasOwnProperty(property) ) {
					if (dataObject[property] != null) {	
						this[property] = dataObject[property];
					}
				} else if (displayLog == true) {
					trace("ERROR: " + getQualifiedClassName(this) + ": VO loading from Array, key : " + property+ " does not exists");
				}
			}
			
		}
		
		public function setServicesParameters(pTableOrView:String, pSearchField:String, pStoredProcedureName:String):void {
			this.tableOrView 		= pTableOrView;
			this.searchField 		= pSearchField;
			this.VOStoredProcedure 	= pStoredProcedureName;
			
		}
		
		public function loadListOfObjectsFromArray(objectList:Array, targetArray:String, objectClass:String):void {
			objectClass = "VOs." + objectClass;
			
			//trace("type of class is " + objectClass);
			var dynamicClass:Class = getDefinitionByName(objectClass) as Class;
			
			for (var t:int = 0; t < objectList.length; t++) {
				
				var inst:Object = new dynamicClass();
				inst.loadDataFromResult(objectList[t]);
				
				this[targetArray].push(inst);
			}
		}
		
		public function loadListOfObjectsFromArrayToArray(objectList:Array, targetArray:Array, objectClass:String):void {
			objectClass = "com.app.vo." + objectClass;
			
			//trace("type of class is " + objectClass);
			var dynamicClass:Class = getDefinitionByName(objectClass) as Class;
			
			for (var t:int = 0; t < objectList.length; t++) {
				
				var inst:Object = new dynamicClass();
				inst.loadDataFromResult(objectList[t]);
				
				this[targetArray].push(inst);
			}
		}
		
		/* Binary Proxy. Pass this function an object with the relevant data and if this object has such property it will be given the value of the object.*/
		/*public function loadDataFromResult(dataObject:Object,  displayLog:Boolean = false):void {
			
			for (var property:String in dataObject) {
				if (displayLog == true) {
					trace("processing property ");
				}
				if (this.hasOwnProperty(property) ) {
					if (dataObject[property] != null) {	
						this[property] = dataObject[property];
					}
				} else {
					trace("ERROR: " + getQualifiedClassName(this) + ": VO loading from Array, key : " + property+ " does not exists");
				}
			}
		}*/
		
		public function loadListOfObjectsFromObject(objectList:Object, targetArray:String, objectClass:String):void {
			objectClass = "VOs." + objectClass;
			
			//trace("type of class is " + objectClass);
			var dynamicClass:Class = getDefinitionByName(objectClass) as Class;
			
			for (var t:int = 0; t < objectList.length; t++) {
				
				var inst:Object = new dynamicClass();
				inst.loadDataFromResult(objectList[t]);
				
				this[targetArray].push(inst);
			}
		}
		
	}
}