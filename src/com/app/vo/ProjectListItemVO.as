package com.app.vo
{
	public class ProjectListItemVO
	{
		public var projectName:String = "";
		public var lastUpdated:Date;
		public var totalTrims:Number = 0;
	}
}