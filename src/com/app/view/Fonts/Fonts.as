		// ActionScript file
		   
	/* Available font list */
	[Embed(source="Manibus Regular.otf",
					fontName = "Manibus Regular",
					mimeType = "application/x-font")]
	public var Manibus_Regular:Class;
	
	[Embed(source="RomanLoveStory.otf",
					fontName = "Roman Love Story",
					mimeType = "application/x-font")]
	public var Roman_Love_Story:Class;
	
	[Embed(source="arial.ttf",
					fontName = "Arial Normal",
					mimeType = "application/x-font")]
	public var Arial:Class;
	
	[Embed(source="arialbd.ttf",
					fontName = "Arial Bold",
					mimeType = "application/x-font")]
	public var ArialB:Class;
	
	[Embed(source="arialbi.ttf",
					fontName = "Arial Bold Italic",
					mimeType = "application/x-font")]
	public var ArialBI:Class;
	
	[Embed(source="ariali.ttf",
					fontName = "Arial Italic",				
					mimeType = "application/x-font")]
	public var ArialI:Class;

	[Embed(source="Pirates.ttf",
					fontName = "Pirates",				
					mimeType = "application/x-font")]
	public var Pirates:Class;

	[Embed(source="impact.ttf",
					fontName = "Impact",				
					mimeType = "application/x-font")]
	public var Impact:Class;

	[Embed(source="Bringtong.ttf",
					fontName = "Bringtong",				
					mimeType = "application/x-font")]
	public var Bringtong:Class;
	
	[Embed(source="cour.ttf",
					fontName = "Courier New",				
					mimeType = "application/x-font")]
	public var Courier_New:Class;

	[Embed(source="calibri.ttf",
					fontName = "Calibri",				
					mimeType = "application/x-font")]
	public var Calibri:Class;
	
	[Embed(source="times.ttf",
					fontName = "Times New Roman",
					mimeType = "application/x-font")]
	public var Times_New_Roman:Class;
	
	[Embed(source="segoesc.ttf",
					fontName = "Segoe Script",
					mimeType = "application/x-font")]
	public var Segoe_Script:Class;
	
	[Embed(source="Pacifico.ttf",
					fontName = "Pacifico",
					mimeType = "application/x-font")]
	public var Pacifico:Class;
	
	[Embed(source="Playball.ttf",
					fontName = "Playball",
					mimeType = "application/x-font")]
	public var Playball:Class;	

	[Embed(source="planetbe.ttf",
						fontName = "planetbe",
						mimeType = "application/x-font")]
	public var planetbe:Class;	

	[Embed(source="KOMIKAB_.ttf",
						fontName = "KOMIKAB_",
						mimeType = "application/x-font")]
	public var KOMIKAB_:Class;	


	[Embed(source="Autobus-Bold.ttf",
							fontName = "Autobus-Bold",
							mimeType = "application/x-font")]
	public var Autobus_Bold:Class;	