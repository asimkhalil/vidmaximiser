package com.app.view.customcomponents
{
	
	import com.app.view.FanPagesDropDownSkin;
	
	import flash.utils.ByteArray;
	
	import mx.events.FlexEvent;
	
	import spark.components.DropDownList;
	import spark.components.Image;
	
	public class FanPageDropDown extends DropDownList
	{
		[SkinPart]
		public var img_fanpageThumb:Image; 
		
		private var _icon:ByteArray;
		
		public function FanPageDropDown()
		{
			super();
			this.setStyle("skinClass",FanPagesDropDownSkin);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,onCReationComplete);
		}
		
		public function set icon(data:ByteArray):void {
			_icon = data;
			if(img_fanpageThumb) {
				img_fanpageThumb.source = _icon; 
			}
		}
		
		private function onCReationComplete(event:FlexEvent):void {
			img_fanpageThumb.source = _icon;		
		}
	}
}