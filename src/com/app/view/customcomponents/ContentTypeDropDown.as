package com.app.view.customcomponents
{
	import com.app.skins.ContentTypeDropDownSkin;
	
	import flash.utils.ByteArray;
	
	import mx.events.FlexEvent;
	
	import spark.components.DropDownList;
	import spark.components.Image;
	
	public class ContentTypeDropDown extends DropDownList
	{
		[SkinPart]
		public var img_fanpageThumb:Image; 
		
		private var _icon:Class;
		
		public function ContentTypeDropDown()
		{
			super();
			this.setStyle("skinClass",ContentTypeDropDownSkin);
			this.addEventListener(FlexEvent.CREATION_COMPLETE,onCReationComplete);
		}
		
		public function set icon(data:Class):void {
			_icon = data;
			if(img_fanpageThumb) {
				img_fanpageThumb.source = _icon;
			}
		}
		
		private function onCReationComplete(event:FlexEvent):void {
			img_fanpageThumb.source = _icon;
		}
	}
}