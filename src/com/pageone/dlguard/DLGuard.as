package com.pageone.dlguard {
	
	import com.pageone.settings.Settings;
	
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import model.ApplicationModel;
	
	import mx.controls.AdvancedDataGrid;
	import mx.controls.Alert;
	import mx.core.FlexGlobals;
	import mx.managers.CursorManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.rpc.mxml.Concurrency;
	import mx.utils.StringUtil;
	
	[Event(name="result", type="mx.rpc.events.ResultEvent")]
	[Event(name="fault", type="mx.rpc.events.FaultEvent")]
	public class DLGuard extends EventDispatcher {
		private var dlGuardLocation:String="http://jayvenka.com/customers/query.php";
		private var dlGuardLocationSingleUser:String="http://commentmaximiser.com/licensequery.php";
		private var service:HTTPService;
		private var trialService:HTTPService;
		public function DLGuard() {
			service=new HTTPService();
			if(ApplicationModel.licenseType == ApplicationModel.SINGLE_USER_LICENSE) {
				service.url=dlGuardLocationSingleUser;
			} else {
				service.url=dlGuardLocation;
			}
			service.concurrency=Concurrency.SINGLE;
			service.addEventListener(ResultEvent.RESULT, dlResult);
			service.addEventListener(FaultEvent.FAULT, dlFault);
		}
		
		private function dlResult(e:ResultEvent):void {
			FlexGlobals.topLevelApplication.processing = false;
			if(ApplicationModel.licenseType == ApplicationModel.MULTI_USER_LICENSE) {
				var arr1:Array=e.result.toString().split(":");
				var arr2:Array = arr1[1].toString().split(";");
	
				if(arr2.length==7 && arr2[0]=="1")
				{
					//Settings.app.prodid=int(arr[2]);
					dispatchEvent(new ResultEvent(ResultEvent.RESULT, false, true, true));
				}
				else 
				{
					dispatchEvent(new ResultEvent(ResultEvent.RESULT, false, true, false));
				}
			} else {
				if(e.result.toString()=="2" || e.result.toString()=="1")
				{
					dispatchEvent(new ResultEvent(ResultEvent.RESULT, false, true, true));
				}
				else 
				{
					dispatchEvent(new ResultEvent(ResultEvent.RESULT, false, true, e.result.toString()));
				}
			}
		}
		
		private function dlFault(e:FaultEvent):void { 
			FlexGlobals.topLevelApplication.processing= false;
			dispatchEvent(e);
		}
		
		public function validate(key:String):AsyncToken {
			FlexGlobals.topLevelApplication.processing= true;
			
			var parameters:Object = new Object();
			
			//Personal License
			
			/*if(ApplicationModel.licenseType == ApplicationModel.MULTI_USER_LICENSE) {
				parameters["r"] = key;
				parameters["p"] = ApplicationModel.version==ApplicationModel.VERSION_LIGHT?"8":"7";
				parameters["k"] = "jai123jai123";
			} else {*/
				parameters["k"] = key;
				parameters["m"] = ApplicationModel.macAddress;
			/*}*/
			
			return service.send(parameters);
		}
	}
}
