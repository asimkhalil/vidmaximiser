package flex.utils
{
	
	import flash.filters.BlurFilter;
	import flash.filters.ColorMatrixFilter;
	import flash.filters.ConvolutionFilter;
	import flash.geom.ColorTransform;
	
	/**
	 * Matrix utility class
	 * @version 1.0
	 * so far added:
	 * - brightness
	 * - contrast
	 * - saturation
	 */
	public class MatrixUtil
	{
		/**
		 * sets brightness value available are -100 ~ 100 @default is 0
		 * @param       value:int   brightness value
		 * @return      ColorMatrixFilter
		 */
		public static function setBrightness(value:Number):ColorMatrixFilter
		{
			value = value*(255/250);
			
			var m:Array = new Array();
			m = m.concat([1, 0, 0, 0, value]);  // red
			m = m.concat([0, 1, 0, 0, value]);  // green
			m = m.concat([0, 0, 1, 0, value]);  // blue
			m = m.concat([0, 0, 0, 1, 0]);      // alpha
			
			return new ColorMatrixFilter(m);
		}
		
		private static const lR:Number = 0.213;
		private static const lG:Number = 0.715;
		private static const lB:Number = 0.072;
		
		public static function adjustHue(value:Number):ColorMatrixFilter
		{
			value = value < -1 ? -1 : value;
			value = value > 1 ? 1 : value;
			
			// convert to radians.
			var v:Number = (180 * value) * (Math.PI/180);
			var cv:Number = Math.cos(v);
			var sv:Number = Math.sin(v);
			
			
			var m:Array = new Array();
			m = m.concat([lR + (cv * (1 - lR)) + (sv * -lR), lG + (cv * -lG) + (sv * -lG), lB + (cv * -lB) + (sv * (1 - lB)), 0, 0,
				lR + (cv * -lR) + (sv * 0.143), lG + (cv * (1 - lG)) + (sv * 0.140), lB + (cv * -lB) + (sv * -0.283), 0, 0,
				lR + (cv * -lR) + (sv * -(1 - lR)), lG + (cv * -lG) + (sv * lG), lB + (cv * (1 - lB)) + (sv * lB), 0, 0,
				0, 0, 0, 1, 0]);
			
			return new ColorMatrixFilter(m);
		} 
		
		public static function applyBlur(value:Number):BlurFilter{
			return new BlurFilter(10,10,value);
		}
		
		/*public static function setRGB(r:int=255,g:int=255,b:int=255):ColorTransform
		{
			var colorTrans:ColorTransform = new ColorTransform();
			colorTrans.redOffset = r;
			colorTrans.blueOffset = g;
			colorTrans.greenOffset = b;
			return colorTrans;
		}*/
		
		/*public static function blueEffect():ColorTransform
		{
			var colorTrans:ColorTransform = new ColorTransform();
			colorTrans.redOffset = 0;
			colorTrans.blueOffset = 255;
			colorTrans.greenOffset = 0;
			return colorTrans;
		}
		
		public static function greenEffect():ColorTransform
		{
			var colorTrans:ColorTransform = new ColorTransform();
			colorTrans.redOffset = 0;
			colorTrans.blueOffset = 0;
			colorTrans.greenOffset = 255;
			return colorTrans;
		}*/
		
		public static function redAndBlueEffect():ColorTransform
		{
			var colorTrans:ColorTransform = new ColorTransform();
			colorTrans.redOffset = 255;
			colorTrans.blueOffset = 255;
			colorTrans.greenOffset = 0;
			return colorTrans;
		}
		
		public static function redAndGreenEffect():ColorTransform
		{
			var colorTrans:ColorTransform = new ColorTransform();
			colorTrans.redOffset = 255;
			colorTrans.blueOffset = 0;
			colorTrans.greenOffset = 255;
			return colorTrans;
		}
		
		public static function blueAndGreenEffect():ColorTransform
		{
			var colorTrans:ColorTransform = new ColorTransform();
			colorTrans.redOffset = 0;
			colorTrans.blueOffset = 255;
			colorTrans.greenOffset = 255;
			return colorTrans;
		}
		
		/**
		 * sets contrast value available are -100 ~ 100 @default is 0
		 * @param       value:int   contrast value
		 * @return      ColorMatrixFilter
		 */
		public static function setContrast(value:Number):ColorMatrixFilter
		{
			value /= 100;
			var s: Number = value + 1;
			var o : Number = 128 * (1 - s);
			
			var m:Array = new Array();
			m = m.concat([s, 0, 0, 0, o]);  // red
			m = m.concat([0, s, 0, 0, o]);  // green
			m = m.concat([0, 0, s, 0, o]);  // blue
			m = m.concat([0, 0, 0, 1, 0]);  // alpha
			
			return new ColorMatrixFilter(m);
		}
		
		/**
		 * sets saturation value available are -100 ~ 100 @default is 0
		 * @param       value:int   saturation value
		 * @return      ColorMatrixFilter
		 */
		public static function setSaturation(value:Number):ColorMatrixFilter
		{
			const lumaR:Number = 0.212671;
			const lumaG:Number = 0.71516;
			const lumaB:Number = 0.072169;
			
			var v:Number = (value/100) + 1;
			var i:Number = (1 - v);
			var r:Number = (i * lumaR);
			var g:Number = (i * lumaG);
			var b:Number = (i * lumaB);
			
			var m:Array = new Array();
			m = m.concat([(r + v), g, b, 0, 0]);    // red
			m = m.concat([r, (g + v), b, 0, 0]);    // green
			m = m.concat([r, g, (b + v), 0, 0]);    // blue
			m = m.concat([0, 0, 0, 1, 0]);          // alpha
			
			return new ColorMatrixFilter(m);
		}
		
		public static function setBlackandWhite(bw:Boolean):ColorMatrixFilter 
		{
			var bwMatrix:Array;
			if(bw) {
				var rLum:Number = 0.2225;
				var gLum:Number = 0.7169;
				var bLum:Number = 0.0606;
				
				bwMatrix = [rLum, gLum, bLum, 0, 0,
					rLum, gLum, bLum, 0, 0,
					rLum, gLum, bLum, 0, 0,
					0, 0, 0, 1, 0];
			}
			return new ColorMatrixFilter(bwMatrix);
		}
		
		public static function setRGB(red:Number=1,green:Number=1,blue:Number=1):ColorMatrixFilter 
		{
			var matrix:Array = new Array();
			matrix=matrix.concat([red,0,0,0,0]);// red
			matrix=matrix.concat([0,green,0,0,0]);// green
			matrix=matrix.concat([0,0,blue,0,0]);// blue
			matrix=matrix.concat([0,0,0,1,0]);// alpha
			return new ColorMatrixFilter(matrix);
		}
		
		public static function setSepia(s:Boolean):ColorMatrixFilter {
			if(s) {
				var sepia:ColorMatrixFilter = new ColorMatrixFilter();
				sepia.matrix = [0.3930000066757202, 0.7689999938011169, 
					0.1889999955892563, 0, 0, 0.3490000069141388, 
					0.6859999895095825, 0.1679999977350235, 0, 0, 
					0.2720000147819519, 0.5339999794960022, 
					0.1309999972581863, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1];
				return sepia;
			}
			return new ColorMatrixFilter();
		}
		
		public static function setBlur(bol:Boolean):BlurFilter {
			if(bol) {
				var blur:BlurFilter = new BlurFilter(10,10);
				return blur;
			}
			return new BlurFilter(0,0);
		}
		
		public static function setGreyScale(bol:Boolean):ColorMatrixFilter {
			if(bol) {
				var red:Number = 0.3086; // luminance contrast value for red
				var green:Number = 0.694; // luminance contrast value for green
				var blue:Number = 0.0820;
				var matrix:Array = new Array();
				matrix = matrix.concat([red, green, blue, 0, 0]); // red
				matrix = matrix.concat([red, green, blue, 0, 0]); // green
				matrix = matrix.concat([red, green, blue, 0, 0]); // blue
				matrix = matrix.concat([0, 0, 0, 1, 0]);    // alpha
				var cmf:ColorMatrixFilter = new ColorMatrixFilter();
				cmf.matrix=matrix; 
				return cmf;
			}
			return new ColorMatrixFilter();
		}
		
		public static function noise(bol:Boolean):ConvolutionFilter {
			if(bol) {
				var matrix:Array = [0, 1, 0,
					1, -4, 1,
					0, 1, 0];
				var convolution:ConvolutionFilter = new ConvolutionFilter();
				convolution.matrixX = 3;
				convolution.matrixY = 3;
				convolution.matrix = matrix;
				convolution.divisor = 1;
				return convolution;
			}
			return new ConvolutionFilter();
		}
		
		public static function sharpen(bol:Boolean):ConvolutionFilter {
			if(bol) {
				var matrix:Array = [-2, -1, 0,
					-1, 1, 1,
					0, 1, 2];
				var convolution:ConvolutionFilter = new ConvolutionFilter();
				convolution.matrixX = 3;
				convolution.matrixY = 3;
				convolution.matrix = matrix;
				convolution.divisor = 1;
				return convolution;
			}
			return new ConvolutionFilter();
		}
		
		public static function sketch(bol:Boolean):ConvolutionFilter {
			if(bol) {
				var matrix:Array = [0,  1, 0,
					1, -4.2, 1,
					0,  1.2, 0];
				var convolution:ConvolutionFilter = new ConvolutionFilter();
				convolution.matrixX = 3;
				convolution.matrixY = 3;
				convolution.matrix = matrix;
				convolution.divisor = 0.3;
				return convolution;
			}
			return new ConvolutionFilter();
		}
	}
}
