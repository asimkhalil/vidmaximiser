package model
{
	
	public class ApplicationModel
	{
		public static var SINGLE_USER_LICENSE:String = "singleUserlicense";
		public static var MULTI_USER_LICENSE:String = "multiUserlicense";
		public static var licenseType:String = SINGLE_USER_LICENSE;
		public static var macAddress:String = "";
	}
}